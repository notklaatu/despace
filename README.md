Really simple BASH script to remove spaces (" ") and, optionally, 
annoying characters (',;:=/\") from filenames.

This script exists because I got tired of typing out the equivalent for-loops,
and I especially got tired of mis-typing the for-loops and moving a directory
full of files into one file called {i} or something stupid like that.

```
   despace foo\ bar            = remove space from filename 'foo bar'
   despace foo\ bar baz\ quux  = remove spaces from files 'foo bar' & 'baz\ quux'
   despace *                   = remove spaces from all files in current directory
   despace /path/to/dir/*      = remove spaces from all files in /path/to/dir
   despace --swap '_' foo\ bar = replace spaces with underscore in 'foo bar'
   despace --tidy *            = remove annoying characters from filenames
   despace --dry-run *         = do not actually change anything
```